/*
 *  Author  :   Pratik Varshney
 *  Date    :   18/04/2015
 *  Program :   Genetic Algorithm
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

#define STRSIZE 31
#define STRMEM (STRSIZE+1)
#define YCOUNT 10
#define MAXSTR 10

#define CMIN -2
#define CMAX 5

#define ci(b,L,cmin,cmax) ((cmin)+(((float)(b)/((int)(1<<(L))-1)*((cmax)-(cmin)))))
#define sqr(a) ((a)*(a))

#define MAXFX 400
#define PROB_MUTATION 0.005
#define THRESHOLD 0.8

int string_size = 6;
int y_count = 4;
int num_strings = 4;

struct item
{
    int sno;
    char sel_str[(2*STRMEM)+1];
    char str_a[STRMEM];
    char str_b[STRMEM];
    int c1_bin;
    float c1;
    int c2_bin;
    float c2;
    float y[YCOUNT];
    float fx;
    float ec;
    int ac;
};

struct tabl
{
    struct item it[MAXSTR];
    float sum;
    float avg;
    float maxm;
};

struct data
{
    int num;
    float x;
    float y;
};

int show_table(struct tabl tab, int iter);
int show_header(int iter);
int show_entry(struct item a, int iter);
int show_footer(float sum, float avg, float maxm);
int show_line();

int load_dataset(struct data dat[YCOUNT]);
int load_str(struct item arr[MAXSTR]);
int bstr_to_int(char str[STRMEM]);
int eval_fitness(struct tabl *tab, struct data dat[YCOUNT]);

int selection(struct tabl *tab);
int crossover(struct tabl *tab);
int mutation(struct tabl *tab);

int main()
{
    struct tabl tab;
    struct data dat[YCOUNT];
    int iter;
    char ch;

    srand(time(NULL));

    load_dataset(dat);
    load_str(tab.it);

    for(iter=1; ;iter++)
    {
        eval_fitness(&tab, dat);
        show_table(tab, iter);

        do
        {
            fflush(stdin);
            fprintf(stderr, "\nShow next iteration? [y/n]: ");
            ch = getchar();
        }while(!(ch=='y' || ch=='n' || ch=='Y' || ch=='N'));

        if(ch=='n' || ch=='N') break;

        //update strings
        selection(&tab);
        crossover(&tab);
        mutation(&tab);
    }

    return 0;
}

int show_header(int iter)
{
    int i, j;
    printf("\n\n\n>>> Iteration = %d\n", iter);
    show_line();
    j=(4*string_size)+6;
    for(i=0; i<j; i++) putchar(' ');
    printf("                                ");
    for(i=0; i<y_count; i++) printf("       ");
    printf("                 Expected         \n");
    for(i=0; i<j; i++) putchar(' ');
    printf("     C1             C2          ");
    for(i=0; i<y_count; i++) printf("       ");
    printf(" f(x)=           count =    Actual\n");
    if(iter==1)
        printf("%-*s  %-*s  ", (2*string_size)+4, "String number", (2*string_size)+1,"String"  );
    else
        printf("%-*s  %-*s  ", (2*string_size)+4, "Selected strings", (2*string_size)+1,"New strings"  );
    printf("(bin.)   C1    (bin.)   C2   ");
    for(i=0; i<y_count; i++) printf("  y%-3d ", i+1);
    printf("%4d-E(yr-yi)^2  f/average  count \n", MAXFX);
    show_line();
    return 0;
}

int show_entry(struct item a, int iter)
{
    int i;

    if(iter==1) printf("%8d%*s  ", a.sno, (2*string_size)-4, " ");
    else printf("%-*s  ", (2*string_size)+4, a.sel_str);

    printf("%*s %*s  %4d   %6.2f  %4d   %6.2f ", string_size, a.str_a, string_size, a.str_b, a.c1_bin, a.c1, a.c2_bin, a.c2);

    for(i=0; i<y_count; i++)
    {
        printf("%6.2f ", a.y[i]);
    }

    printf("%8.2f        %5.2f       %-6d\n", a.fx, a.ec, a.ac);
    return 0;
}

int show_footer(float sum, float avg, float maxm)
{
    printf("\n");
    printf("%*s %8.2f\n", 37+(4*string_size)+(7*y_count), "Sum", sum);
    printf("%*s %8.2f\n", 37+(4*string_size)+(7*y_count), "Average", avg);
    printf("%*s %8.2f\n", 37+(4*string_size)+(7*y_count), "Maximum", maxm);

    show_line();

    return 0;
}

int show_line()
{
    int i, j;
    j=(4*string_size)+6;
    for(i=0; i<j; i++) putchar('-');
    printf("--------------------------------");
    for(i=0; i<y_count; i++) printf("-------");
    printf("----------------------------------\n");
    return 0;
}

int load_dataset(struct data dat[YCOUNT])
{
    char ch;
    int i;
    FILE *fp;
    char fname[] = "input_dataset.txt";

    fp = fopen(fname, "r");
    if(fp==NULL)
    {
        fprintf(stderr, "\nError opening file: %s\n", fname);
        return -1;
    }

    printf("\n>>> Dataset\n");
    printf("--------------------------------\n");

    //skip header
    do
    {
        ch = fgetc(fp);
        putchar(ch);
    }while(ch != '\n');
    printf("--------------------------------\n");

    //read entries
    for(i=0; !feof(fp); i++)
    {
        fscanf(fp, "%d%f%f", &(dat[i].num), &(dat[i].x), &(dat[i].y));
        printf("\t%d\t%.1f\t%.1f\n", dat[i].num, dat[i].x, dat[i].y);
    }
    y_count = i;
    printf("--------------------------------\n");
    printf("\n\n");
    fclose(fp);
    printf("+++ Dataset Size = %d\n", y_count);
    return 0;
}

int load_str(struct item arr[MAXSTR])
{
    int i;
    FILE *fp;
    char fname[] = "input_string.txt";
    fp = fopen(fname, "r");
    if(fp==NULL)
    {
        fprintf(stderr, "\nError opening file: %s\n", fname);
        return -1;
    }

    arr[0].str_a[0]='\0';
    for(i=0; !feof(fp); i++)
    {
        fscanf(fp, "%s%s", arr[i].str_a, arr[i].str_b);
    }
    num_strings = i;
    string_size = strlen(arr[0].str_a);
    printf("+++ String Size = %d\n", string_size);
    printf("+++ Number of Strings = %d\n", num_strings);
    fclose(fp);
    return 0;
}

int bstr_to_int(char str[STRMEM])
{
    int i, len, res=0, p;
    len = strlen(str);
    p=(1<<(len-1));
    for(i=0; i<len; i++)
    {
        if(str[i]=='1') res += p;
        p = p >> 1;
    }
    return res;
}

int eval_fitness(struct tabl *tab, struct data dat[YCOUNT])
{
    int i, j;

    tab->sum = 0.0;
    for(i=0; i<num_strings; i++)
    {
        tab->it[i].sno = i+1;
        tab->it[i].c1_bin = bstr_to_int(tab->it[i].str_a);
        tab->it[i].c2_bin = bstr_to_int(tab->it[i].str_b);
        tab->it[i].c1 = ci(tab->it[i].c1_bin, string_size, CMIN, CMAX);
        tab->it[i].c2 = ci(tab->it[i].c2_bin, string_size, CMIN, CMAX);
        tab->it[i].fx = MAXFX;
        for(j=0; j<y_count; j++)
        {
            tab->it[i].y[j] = (tab->it[i].c1 * dat[j].x) + tab->it[i].c2;
            tab->it[i].fx -= sqr((dat[j].y) - (tab->it[i].y[j]));
        }

        if(i==0) tab->maxm = tab->it[i].fx;
        else if(tab->maxm < tab->it[i].fx) tab->maxm = tab->it[i].fx;

        tab->sum += tab->it[i].fx;

        tab->it[i].ac = 1;
    }
    tab->avg = tab->sum / num_strings;
    for(i=0; i<num_strings; i++) tab->it[i].ec = tab->it[i].fx / tab->avg;

    //Calculation of actual count
    int extinct=0, alive=0, temp;
    int str_no[MAXSTR];
    for(i=0; i<num_strings; i++)
    {
        if(tab->it[i].ec < THRESHOLD)
        {
            extinct++;
            tab->it[i].ac=0;
        }
        else
        {
            tab->it[i].ac=1;
            str_no[alive]=i;
            alive++;
        }
    }
    if(extinct>0)
    {
        //sort alive descending
        for(i=0; i<alive; i++)
        {
            for(j=i+1; j<alive; j++)
            {
                if(tab->it[str_no[i]].ec < tab->it[str_no[j]].ec)
                {
                    //swap
                    temp = str_no[i];
                    str_no[i] = str_no[j];
                    str_no[j] = temp;
                }
            }
        }
        for(i=0; i<extinct; i++)
        {
            tab->it[str_no[i%alive]].ac++;
        }
    }


    return 0;
}

int show_table(struct tabl tab, int iter)
{
    int i;
    show_header(iter);
    for(i=0; i<num_strings; i++)
    {
        show_entry(tab.it[i], iter);
    }
    show_footer(tab.sum, tab.avg, tab.maxm);
    return 0;
}

int selection(struct tabl *tab)
{
    int i, j, k;
    int arr[MAXSTR];

    //select indices for mating pool
    k=0;
    for(i=0; i<num_strings; i++)
    {
        for(j=0; j<(tab->it[i].ac); j++)
        {
            arr[k]=i;
            k++;
        }
    }

    //shuffle index
    for(i=0; i<num_strings; i++)
    {
        j=rand()%num_strings;
        k=arr[i];
        arr[i]=arr[j];
        arr[j]=k;
    }

    //generate children (mating pool)
    for(i=0; i<num_strings; i++)
    {
        tab->it[i].sel_str[0]='\0';
        strcat(tab->it[i].sel_str, tab->it[arr[i]].str_a);
        strcat(tab->it[i].sel_str, tab->it[arr[i]].str_b);
    }
    return 0;
}

int crossover(struct tabl *tab)
{
    int i, j;
    char ta[(2*STRMEM)+1], tb[(2*STRMEM)+1];
    for(i=1; i<num_strings; i+=2)
    {
        //crossover point
        j=rand()%(2*string_size);

        //exchange
        strncpy(ta, tab->it[i-1].sel_str, j);
        ta[j]='\0';
        strncat(ta, (tab->it[i].sel_str)+j, (2*string_size)-j);
        //ta[(2*string_size)]='\0';

        strncpy(tb, tab->it[i].sel_str, j);
        tb[j]='\0';
        strncat(tb, (tab->it[i-1].sel_str)+j, (2*string_size)-j);
        //tb[(2*string_size)]='\0';

        //copy
        strncpy(tab->it[i-1].str_a, ta, string_size);
        tab->it[i-1].str_a[string_size]='\0';
        strncpy(tab->it[i-1].str_b, ta+string_size, string_size);
        tab->it[i-1].str_b[string_size]='\0';

        strncpy(tab->it[i].str_a, tb, string_size);
        tab->it[i].str_a[string_size]='\0';
        strncpy(tab->it[i].str_b, tb+string_size, string_size);
        tab->it[i].str_b[string_size]='\0';

        //mark
        strncpy(ta, tab->it[i-1].sel_str, j);
        ta[j]='\0';
        strcat(ta, ":");
        strncat(ta, (tab->it[i-1].sel_str)+j, (2*string_size)-j);
        strcpy(tab->it[i-1].sel_str, ta);

        strncpy(ta, tab->it[i].sel_str, j);
        ta[j]='\0';
        strcat(ta, ":");
        strncat(ta, (tab->it[i].sel_str)+j, (2*string_size)-j);
        strcpy(tab->it[i].sel_str, ta);

    }
    return 0;
}

int mutation(struct tabl *tab)
{
    int i, j;
    double r;
    for(i=0; i<num_strings; i++)
    {
        for(j=0; j<string_size; j++)
        {
            r=((double)(rand()%1000))/1000;
            if(r<PROB_MUTATION)
            {
                tab->it[i].str_a[j] = (tab->it[i].str_a[j]=='0')?'1':'0';
                printf("*** Mutation in C1 of string %d, bit %d (from left)\n", i+1, j+1);
            }

            r=((double)(rand()%1000))/1000;
            if(r<PROB_MUTATION)
            {
                tab->it[i].str_b[j] = (tab->it[i].str_b[j]=='0')?'1':'0';
                printf("*** Mutation in C2 of string %d, bit %d (from left)\n", i+1, j+1);
            }
        }
    }
    return 0;
}
